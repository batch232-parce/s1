package com.zuitt.example;

import java.util.Scanner;

public class Activity {
    public static void main (String[] args){
        Scanner object = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = object.nextLine();

        System.out.println("Last Name:");
        String secondName = object.nextLine();

        System.out.println("First Subject Grade:");
        double subGrade1 = object.nextDouble();

        System.out.println("Second Subject Grade:");
        double subGrade2 = object.nextDouble();

        System.out.println("Third Subject Grade:");
        double subGrade3 = object.nextDouble();

        double average = (subGrade1 + subGrade2 + subGrade3) / 3;
        System.out.println("Good day, " + firstName + " " + secondName + ".");
        System.out.println("Your grade average is: " + average);
    }
}
